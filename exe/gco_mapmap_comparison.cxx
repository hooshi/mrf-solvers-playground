#define GCO_ENERGY_TYPE double
#define GCO_ENERGY_TERM_TYPE double
#define GCO_PROVIDE_IMPLEMENTATION
//#include "gco/GCoptimization.h"
#include "gconew/GCoptimization.h"

#include "mrf_solver/mrf_solvers.hxx"

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

//#include "mapmap/full.h"
//#include "tbb/task_scheduler_init.h"


// This is buggy according to Nicholas.
// Use what polycut is using.
struct GCO_unary_cost_context
{
  int n_label;
  int *data;
};

GCO_ENERGY_TYPE
gco_unary_cost(int p, int l, void *data)
{
  GCO_unary_cost_context *myData = (GCO_unary_cost_context *)data;
  int numLab = myData->n_label;

  return GCO_ENERGY_TYPE(myData->data[p * numLab + l]);
}

class GCO_grid_unary_cost_functor : public GCoptimization::DataCostFunctor
{
public:
  typedef GCoptimization::EnergyType Energy_type;
  typedef GCoptimization::SiteID Site_index_type;
  typedef GCoptimization::LabelID Label_index_type;

  GCO_grid_unary_cost_functor(int n_labels, int *data) : _n_labels(n_labels), _data(data) {}

  Energy_type
  compute(Site_index_type sid, Label_index_type lid)
  {
    return Energy_type(_data[sid * _n_labels + lid]);
  }

private:
  int _n_labels;
  int *_data;
};

struct GCO_pairwise_cost_context
{
};
GCO_ENERGY_TYPE
gco_pairwise_cost(int p1, int p2, int l1, int l2, void *)
{
  if(p1 < 4)
    {
      return GCO_ENERGY_TYPE((l1 != l2) * 3);
    }
  else
    {
      if((l1 - l2) * (l1 - l2) <= 4)
        return GCO_ENERGY_TYPE((l1 - l2) * (l1 - l2));
      else
        return GCO_ENERGY_TYPE(4);
    }
}

class GCO_grid_pairwise_cost_functor : public GCoptimization::SmoothCostFunctor
{
public:
  typedef GCoptimization::EnergyType Energy_type;
  typedef GCoptimization::SiteID Site_index_type;
  typedef GCoptimization::LabelID Label_index_type;

  Energy_type
  compute(Site_index_type sid1, Site_index_type sid2, Label_index_type lid1, Label_index_type lid2)
  {
    if(sid1 < 4)
      {
        return Energy_type((lid1 != lid2) * 3);
      }
    else
      {
        if((lid1 - lid2) * (lid1 - lid2) <= 4)
          return Energy_type((lid1 - lid2) * (lid1 - lid2));
        else
          return Energy_type(4);
      }
  }
};

////////////////////////////////////////////////////////////////////////////////
// in this version, set data and smoothness terms using arrays
// grid neighborhood structure is assumed
////////////////////////////////////////////////////////////////////////////////

void
gco_grid_demo(int width, int height, int num_pixels, int num_labels)
{

  int *result = new int[num_pixels]; // stores result of optimization

  // first set up the array for data costs
  int *data = new int[num_pixels * num_labels];
  for(int i = 0; i < num_pixels; i++)
    {
      for(int l = 0; l < num_labels; l++)
        {
          if(i < 25)
            {
              if(l == 0)
                data[i * num_labels + l] = 0;
              else
                data[i * num_labels + l] = 10;
            }
          else
            {
              if(l == 5)
                data[i * num_labels + l] = 0;
              else
                data[i * num_labels + l] = 10;
            }
        }
    }


  try
    {
      GCoptimizationGridGraph *gc = new GCoptimizationGridGraph(width, height, num_labels);

      // set up the needed data to pass to function for the data costs

      //
      // FUNCTIONS
      //
      // GCO_unary_cost_context unary_cost_ctx;
      // unary_cost_ctx.data = data;
      // unary_cost_ctx.n_label = num_labels;
      // gc->setDataCost(&gco_unary_cost, &unary_cost_ctx);
      // gc->setSmoothCost(&gco_pairwise_cost);
      // smoothness comes from function pointer

      //
      // FUNCTORS
      //
      GCO_grid_unary_cost_functor unary_functor(num_labels, data);
      gc->setDataCostFunctor(&unary_functor);
      GCO_grid_pairwise_cost_functor piarwise_cost;
      gc->setSmoothCostFunctor(&piarwise_cost);


      std::cout << "Before optimization energy is " << gc->compute_energy() << std::endl;
      for(int i = 0; i < num_pixels; i++)
        {
          printf("label %d: %d\n", i, gc->whatLabel(i));
          // result[i] = gc->whatLabel(i);
        }
      printf("========================");

      // gc->expansion(2); // run expansion for 2 iterations. For swap use gc->swap(num_iterations);
      gc->swap(-1);
      std::cout << "After optimization energy is " << gc->compute_energy() << std::endl;

      for(int i = 0; i < num_pixels; i++)
        {
          printf("label %d: %d\n", i, gc->whatLabel(i));
          // result[i] = gc->whatLabel(i);
        }

      delete gc;
    }
  catch(GCException e)
    {
      e.Report();
    }

  delete[] result;
  delete[] data;
}


////////////////////////////////////////////////////////////////////////////////
// GCO UNSTRUCTURED GRAPH DEMO
////////////////////////////////////////////////////////////////////////////////

void
gco_graph_demo(int width, int height, int num_pixels, int num_labels)
{

  int *result = new int[num_pixels]; // stores result of optimization

  // first set up the array for data costs
  int *data = new int[num_pixels * num_labels];
  for(int i = 0; i < num_pixels; i++)
    {
      for(int l = 0; l < num_labels; l++)
        {
          if(i < 25)
            {
              if(l == 0)
                data[i * num_labels + l] = 0;
              else
                data[i * num_labels + l] = 10;
            }
          else
            {
              if(l == 5)
                data[i * num_labels + l] = 0;
              else
                data[i * num_labels + l] = 10;
            }
        }
    }


  try
    {

      // Create the graph
      std::unique_ptr<GCoptimizationGeneralGraph> gc(new GCoptimizationGeneralGraph(num_pixels, num_labels));

      //
      // Setup connectivity
      //
      // Horizontal
      for(int y = 0; y < height; y++)
        for(int x = 1; x < width; x++)
          {
            int p1 = x - 1 + y * width;
            int p2 = x + y * width;
            gc->setNeighbors(p1, p2);
          }

      // next set up vertical neighbors
      for(int y = 1; y < height; y++)
        for(int x = 0; x < width; x++)
          {
            int p1 = x + (y - 1) * width;
            int p2 = x + y * width;
            gc->setNeighbors(p1, p2);
          }


      // set up the needed data to pass to function for the data costs

      //
      // FUNCTIONS
      //
      // GCO_unary_cost_context unary_cost_ctx;
      // unary_cost_ctx.data = data;
      // unary_cost_ctx.n_label = num_labels;
      // gc->setDataCost(&gco_unary_cost, &unary_cost_ctx);
      // gc->setSmoothCost(&gco_pairwise_cost);
      // smoothness comes from function pointer

      //
      // FUNCTORS
      //
      GCO_grid_unary_cost_functor unary_functor(num_labels, data);
      gc->setDataCostFunctor(&unary_functor);
      GCO_grid_pairwise_cost_functor piarwise_cost;
      gc->setSmoothCostFunctor(&piarwise_cost);


      std::cout << "Before optimization energy is " << gc->compute_energy() << std::endl;
      for(int i = 0; i < num_pixels; i++)
        {
          printf("label %d: %d\n", i, gc->whatLabel(i));
          // result[i] = gc->whatLabel(i);
        }
      printf("========================");

      // gc->expansion(2); // run expansion for 2 iterations. For swap use gc->swap(num_iterations);
      gc->swap(-1);
      std::cout << "After optimization energy is " << gc->compute_energy() << std::endl;

      for(int i = 0; i < num_pixels; i++)
        {
          printf("label %d: %d\n", i, gc->whatLabel(i));
          // result[i] = gc->whatLabel(i);
        }
    }
  catch(GCException e)
    {
      e.Report();
    }

  delete[] result;
  delete[] data;
}



////////////////////////////////////////////////////////////////////////////////
// MAP MAP SOLVER
////////////////////////////////////////////////////////////////////////////////
#if 0
void
mapmap_demo(int width, int height, int num_pixels, int n_labels)
{

  // int num_threads = 1;
  // tbb::task_scheduler_init schedule(num_threads);

  // Cost type and it's size
  using cost_t = double;
  constexpr mapmap::uint_t simd_w = mapmap::sys_max_simd_width<cost_t>();
  std::cout << "Using SIMD width = " << simd_w << std::endl;
  using unary_t = mapmap::UnaryTable<cost_t, simd_w>;
  using pairwise_t = mapmap::PairwiseTable<cost_t, simd_w>;


  /* pointer to data structures */
  std::unique_ptr<mapmap::Graph<cost_t>> graph;
  std::unique_ptr<mapmap::LabelSet<cost_t, simd_w>> label_set;
  std::vector<std::unique_ptr<unary_t>> unaries;
  std::vector<std::unique_ptr<pairwise_t>> pairwise;

  /* termination criterion and control flow */
  std::unique_ptr<mapmap::TerminationCriterion<cost_t, simd_w>> terminate;
  mapmap::mapMAP_control ctr;

  /* solver instance */
  mapmap::mapMAP<cost_t, simd_w> mapmap_solver;

  /* path to dataset */
  const uint32_t num_nodes = num_pixels;
  const uint32_t num_edges = (width - 1) * height + (height - 1) * width;
  const uint32_t num_labels = n_labels;
  std::cout << "Nodes: " << num_nodes << std::endl;
  std::cout << "Edges: " << num_edges << std::endl;
  std::cout << "Labels: " << num_labels << std::endl;

  //
  // Setup connectivity
  //
  graph.reset(new mapmap::Graph<cost_t>(num_nodes));

  // Horizontal
  for(int y = 0; y < height; y++)
    {
      for(int x = 1; x < width; x++)
        {
          int p1 = x - 1 + y * width;
          int p2 = x + y * width;
          graph->add_edge(p1, p2, 1);
        }
    }

  // next set up vertical neighbors
  for(int y = 1; y < height; y++)
    {
      for(int x = 0; x < width; x++)
        {
          int p1 = x + (y - 1) * width;
          int p2 = x + y * width;
          graph->add_edge(p1, p2, 1);
        }
    }

  graph->update_components();

  //
  // Labels available for each vertex
  //
  label_set.reset(new mapmap::LabelSet<cost_t, simd_w>(num_nodes, false));
  {
    std::vector<mapmap::_iv_st<cost_t, simd_w>> fixed_labels = { 0, 1, 2, 3, 4, 5, 6 };
    for(int pid = 0; pid < num_pixels; ++pid)
      {
        label_set->set_label_set_for_node(pid, fixed_labels);
      }
  }

  //
  // Unary costs for each label
  //
  unaries.resize(num_pixels);
  for(int i = 0; i < num_pixels; i++)
    {
      std::vector<mapmap::_s_t<cost_t, simd_w>> costs(num_labels);

      for(int l = 0; l < num_labels; l++)
        {
          if(i < 25)
            {
              if(l == 0)
                costs[l] = 0;
              else
                costs[l] = 10;
            }
          else
            {
              if(l == 5)
                costs[l] = 0;
              else
                costs[l] = 10;
            }
        }

      unaries[i].reset(new unary_t(i, label_set.get()));
      unaries[i]->set_costs(costs);
    }

  //
  // Pairwise costs for each label
  //
  {
    int edgeid = 0;
    pairwise.resize(num_edges);
    for(int y = 0; y < height; y++)
      {
        for(int x = 1; x < width; x++)
          {
            int p1 = x - 1 + y * width;
            int p2 = x + y * width;

            std::vector<mapmap::_s_t<cost_t, simd_w>> costs(num_labels * num_labels);

            for(int l1 = 0; l1 < num_labels; ++l1)
              {
                for(int l2 = 0; l2 < num_labels; ++l2)
                  {
                    costs[l1 * num_labels + l2] = gco_pairwise_cost(p1, p2, l1, l2, NULL);
                  }
              }
            pairwise[edgeid].reset(new pairwise_t(p1, p2, label_set.get(), costs));
            ++edgeid;
          }
      }

    // next set up vertical neighbors
    for(int y = 1; y < height; y++)
      {
        for(int x = 0; x < width; x++)
          {
            int p1 = x + (y - 1) * width;
            int p2 = x + y * width;
            std::vector<mapmap::_s_t<cost_t, simd_w>> costs(num_labels * num_labels);

            for(int l1 = 0; l1 < num_labels; ++l1)
              {
                for(int l2 = 0; l2 < num_labels; ++l2)
                  {
                    costs[l1 * num_labels + l2] = gco_pairwise_cost(p1, p2, l1, l2, NULL);
                  }
              }
            pairwise[edgeid].reset(new pairwise_t(p1, p2, label_set.get(), costs));
            ++edgeid;
          }
      }
  }

  //
  // Termination criterion
  //
  terminate.reset(new mapmap::StopWhenReturnsDiminish<cost_t, simd_w>(5, 0.0001));

  //
  // Set up the solver
  //
  mapmap_solver.set_graph(graph.get());
  mapmap_solver.set_label_set(label_set.get());
  for(int n = 0; n < num_nodes; ++n)
    {
      mapmap_solver.set_unary(n, unaries[n].get());
    }
  for(int n = 0; n < num_edges; ++n)
    {
      mapmap_solver.set_pairwise(n, pairwise[n].get());
    }
  mapmap_solver.set_termination_criterion(terminate.get());
  std::cout << "Finished loading dataset." << std::endl;

  //
  // Settings
  //

  /* create (optional) control flow settings */
  ctr.use_multilevel = true;
  ctr.use_spanning_tree = true;
  ctr.use_acyclic = true;
  ctr.spanning_tree_multilevel_after_n_iterations = 5;
  ctr.force_acyclic = true;
  ctr.min_acyclic_iterations = 5;
  ctr.relax_acyclic_maximal = true;
  ctr.tree_algorithm = mapmap::LOCK_FREE_TREE_SAMPLER;

  /* set to true and select a seed for (serial) deterministic sampling */
  ctr.sample_deterministic = false;
  ctr.initial_seed = 548923723;

  //
  // Run the solver
  //
  std::vector<mapmap::_iv_st<cost_t, simd_w>> solution;
  mapmap_solver.optimize(solution, ctr);

  //
  // Extract the labels
  //
  std::vector<mapmap::_iv_st<cost_t, simd_w>> labeling(num_nodes);
  for(uint32_t n = 0; n < num_nodes; ++n)
    {
      labeling[n] = label_set->label_from_offset(n, solution[n]);
      printf("Node %d: l %d s %d \n", n, labeling[n], solution[n]);
    }
}
#endif

////////////////////////////////////////////////////////////////////////////////
// DGP INTErFACE WITH MAPMAP
////////////////////////////////////////////////////////////////////////////////
void
mapmap_demo_2(int width, int height, int num_pixels, int num_labels)
{

  // first set up the array for data costs
  int *data = new int[num_pixels * num_labels];
  for(int i = 0; i < num_pixels; i++)
    {
      for(int l = 0; l < num_labels; l++)
        {
          if(i < 25)
            {
              if(l == 0)
                data[i * num_labels + l] = 0;
              else
                data[i * num_labels + l] = 10;
            }
          else
            {
              if(l == 5)
                data[i * num_labels + l] = 0;
              else
                data[i * num_labels + l] = 10;
            }
        }
    }


  try
    {

      // Create the graph
      std::unique_ptr<dgp::mrf::MRF_solver_base> solver(
          dgp::mrf::build_mrf_solver(dgp::mrf::MRF_SOLVER_MAPMAP, num_pixels, num_labels));

      //
      // Setup connectivity
      //
      // Horizontal
      for(int y = 0; y < height; y++)
        for(int x = 1; x < width; x++)
          {
            int p1 = x - 1 + y * width;
            int p2 = x + y * width;
            solver->add_edge(p1, p2);
          }

      // next set up vertical neighbors
      for(int y = 1; y < height; y++)
        for(int x = 0; x < width; x++)
          {
            int p1 = x + (y - 1) * width;
            int p2 = x + y * width;
            solver->add_edge(p1, p2);
          }
      solver->finalize_graph();


      //
      // Costs
      //
      for(int l = 0; l < num_labels; ++l)
        {
          std::vector<double> cost;
          cost.reserve(num_pixels);
          std::vector<int> indices;
          indices.reserve(num_pixels);
          for(int v = 0; v < num_pixels; ++v)
            {
              indices.push_back(v);
              cost.push_back(data[v * num_labels + l]);
            }
          solver->set_unary_costs(l, indices, cost);
        }
      solver->set_binary_costs(&gco_pairwise_cost, NULL);
      solver->finalize_cost();


      // gc->expansion(2); // run expansion for 2 iterations. For swap use gc->swap(num_iterations);
      std::vector<int> result;
      double obj;
      solver->solve(result, obj);
      std::cout << "After optimization energy is " << obj << std::endl;

      for(int i = 0; i < num_pixels; i++)
        {
          printf("label %d: %d\n", i, result[i]);
        }
    }
  catch(GCException e)
    {
      e.Report();
    }

  delete[] data;
}


////////////////////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////////////////////

int
main(int argc, char **argv)
{
  int width = 10;
  int height = 5;
  int num_pixels = width * height;
  int num_labels = 7;

  // smoothness and data costs are set up using functions
  gco_grid_demo(width, height, num_pixels, num_labels);
  gco_graph_demo(width, height, num_pixels, num_labels);
  // mapmap_demo(width, height, num_pixels, num_labels);
  mapmap_demo_2(width, height, num_pixels, num_labels);

  printf("\n  Finished %d (%d) clock per sec %d", int(clock() / CLOCKS_PER_SEC), int(clock()), int(CLOCKS_PER_SEC));
  printf("\n");

  return 0;
}


/////////////////////////////////////////////////////////////////////////////////
