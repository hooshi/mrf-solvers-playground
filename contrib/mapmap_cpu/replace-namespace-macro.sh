find -name '*.h' -exec sed -i 's/NS_MAPMAP_BEGIN/namespace\ mapmap {/g' {} \;
find -name '*.h' -exec  sed -i 's/NS_MAPMAP_END/}/g' {} \;
find -name '*.cpp' -exec sed -i 's/NS_MAPMAP_BEGIN/namespace\ mapmap {/g' {} \;
find -name '*.cpp' -exec  sed -i 's/NS_MAPMAP_END/}/g' {} \;
